#include <signal.h>
#include <open62541.h>
#include <unistd.h>

#define SERVER_HOST "opc.tcp://server:4840"
#define SERVER_TIME_NODE_ID 1
#define SERVER_TIME_ID "current-time-datasource"

UA_Int32 n = 42;

int main(void) {
  UA_Client *client = UA_Client_new();
  UA_ClientConfig_setDefault(UA_Client_getConfig(client));
  UA_StatusCode retval = UA_Client_connect(client, SERVER_HOST);
  
  if (retval != UA_STATUSCODE_GOOD) {
    UA_Client_delete(client);
    return (int)retval;
  }

  UA_Variant serverTime;
  UA_Variant_init(&serverTime);

  const UA_NodeId nodeServerTimeId = UA_NODEID_STRING(SERVER_TIME_NODE_ID, SERVER_TIME_ID);

  UA_Variant theAnswer;
  UA_Variant_setScalar(&theAnswer, &n, &UA_TYPES[UA_TYPES_INT32]);

  const UA_NodeId nodeTheAnswerId = UA_NODEID_STRING(1, "the.answer");
  
  while(1) {
    retval = UA_Client_readValueAttribute(client, nodeServerTimeId, &serverTime);

    if(retval == UA_STATUSCODE_GOOD &&
        UA_Variant_hasScalarType(&serverTime, &UA_TYPES[UA_TYPES_DATETIME])) {
        UA_DateTime raw_date = *(UA_DateTime *) serverTime.data;
        UA_DateTimeStruct dts = UA_DateTime_toStruct(raw_date);
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "server date: %u-%u-%u %u:%u:%u.%03u\n",
                    dts.day, dts.month, dts.year, dts.hour, dts.min, dts.sec, dts.milliSec);
    }

    retval = UA_Client_readValueAttribute(client, nodeTheAnswerId, &theAnswer);

    if (retval == UA_STATUSCODE_GOOD &&
        UA_Variant_hasScalarType(&theAnswer, &UA_TYPES[UA_TYPES_INT32])) {
        UA_Int32 val = *(UA_Int32 *) theAnswer.data;
        UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "the answer: %u\n",
                    val);
    }

    sleep(1);
  }

  UA_Variant_clear(&serverTime);
  UA_Client_delete(client); 
  return EXIT_SUCCESS;
}
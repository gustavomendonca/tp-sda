#include "currentTime.h"

void updateCurrentTime(UA_Server *server) {
  UA_DateTime now = UA_DateTime_now();
  UA_Variant value;
  UA_Variant_setScalar(&value, &now, &UA_TYPES[UA_TYPES_DATETIME]);
  UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-value-callback");
  UA_Server_writeValue(server, currentNodeId, value);
}

void addCurrentTimeVariable(UA_Server *server) {
  UA_DateTime now = 0;
  UA_VariableAttributes attr = UA_VariableAttributes_default;
  attr.displayName = UA_LOCALIZEDTEXT("en-US", "Current time - value callback");
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
  UA_Variant_setScalar(&attr.value, &now, &UA_TYPES[UA_TYPES_DATETIME]);

  UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-value-callback");
  UA_QualifiedName currentName = UA_QUALIFIEDNAME(1, "current-time-value-callback");
  UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
  UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
  UA_NodeId variableTypeNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE);
  UA_Server_addVariableNode(server, currentNodeId, parentNodeId,
                            parentReferenceNodeId, currentName,
                            variableTypeNodeId, attr, NULL, NULL);

  updateCurrentTime(server);
}

UA_StatusCode readCurrentTime(UA_Server *server,
                const UA_NodeId *sessionId, void *sessionContext,
                const UA_NodeId *nodeId, void *nodeContext,
                UA_Boolean sourceTimeStamp, const UA_NumericRange *range,
                UA_DataValue *dataValue) {
    UA_DateTime now = UA_DateTime_now();
    UA_Variant_setScalarCopy(&dataValue->value, &now,
                             &UA_TYPES[UA_TYPES_DATETIME]);
    dataValue->hasValue = true;
    return UA_STATUSCODE_GOOD;
}

UA_StatusCode writeCurrentTime(UA_Server *server,
                const UA_NodeId *sessionId, void *sessionContext,
                const UA_NodeId *nodeId, void *nodeContext,
                const UA_NumericRange *range, const UA_DataValue *data) {
  UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
              "Changing the system time is not implemented");
  return UA_STATUSCODE_BADINTERNALERROR;
}

void addCurrentTimeDataSourceVariable(UA_Server *server) {
  UA_VariableAttributes attr = UA_VariableAttributes_default;
  attr.displayName = UA_LOCALIZEDTEXT("en-US", "Current time - data source");
  attr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

  UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-datasource");
  UA_QualifiedName currentName = UA_QUALIFIEDNAME(1, "current-time-datasource");
  UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
  UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
  UA_NodeId variableTypeNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE);

  UA_DataSource timeDataSource;
  timeDataSource.read = readCurrentTime;
  timeDataSource.write = writeCurrentTime;
  UA_Server_addDataSourceVariableNode(server, currentNodeId, parentNodeId,
                                      parentReferenceNodeId, currentName,
                                      variableTypeNodeId, attr,
                                      timeDataSource, NULL, NULL);
}

void beforeReadTime(UA_Server *server,
               const UA_NodeId *sessionId, void *sessionContext,
               const UA_NodeId *nodeid, void *nodeContext,
               const UA_NumericRange *range, const UA_DataValue *data) {
    updateCurrentTime(server);
}

void afterWriteTime(UA_Server *server,
              const UA_NodeId *sessionId, void *sessionContext,
              const UA_NodeId *nodeId, void *nodeContext,
              const UA_NumericRange *range, const UA_DataValue *data) {
  UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
              "The variable was updated");
}

void addValueCallbackToCurrentTimeVariable(UA_Server *server) {
  UA_NodeId currentNodeId = UA_NODEID_STRING(1, "current-time-value-callback");
  UA_ValueCallback callback ;
  callback.onRead = beforeReadTime;
  callback.onWrite = afterWriteTime;
  UA_Server_setVariableNode_valueCallback(server, currentNodeId, callback);
}
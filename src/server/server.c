#include <signal.h>
#include <open62541.h>
#include "currentTime.h"
#include "theanswer.h"

UA_Boolean running = true;

void signalHandler(int sig) {
  running = false;
}

int main(int argc, char** argv) {
  signal(SIGINT, signalHandler); // trata ctrl+c

  // cria servidor na porta 4840
  UA_ServerConfig *config = UA_ServerConfig_new_default();
  UA_Server *server = UA_Server_new(config);

  // adiciona currentTime
  addCurrentTimeVariable(server);
  addValueCallbackToCurrentTimeVariable(server);
  addCurrentTimeDataSourceVariable(server);

  // adiciona variavel numerica
  addTheAnswerVariable(server);


  // executa loop do servidor
  UA_StatusCode status = UA_Server_run(server, &running);
  UA_Server_delete(server);
  UA_ServerConfig_delete(config);

  return status;
}

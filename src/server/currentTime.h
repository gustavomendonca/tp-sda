#ifndef CURRENTTIME_H
#define CURRENTTIME_H

#include<open62541.h>

void updateCurrentTime(UA_Server *server);

void addCurrentTimeVariable(UA_Server *server);

UA_StatusCode readCurrentTime(UA_Server *server,
                const UA_NodeId *sessionId, void *sessionContext,
                const UA_NodeId *nodeId, void *nodeContext,
                UA_Boolean sourceTimeStamp, const UA_NumericRange *range,
                UA_DataValue *dataValue);

UA_StatusCode writeCurrentTime(UA_Server *server,
                const UA_NodeId *sessionId, void *sessionContext,
                const UA_NodeId *nodeId, void *nodeContext,
                const UA_NumericRange *range, const UA_DataValue *data);

void addCurrentTimeDataSourceVariable(UA_Server *server);

void beforeReadTime(UA_Server *server,
               const UA_NodeId *sessionId, void *sessionContext,
               const UA_NodeId *nodeid, void *nodeContext,
               const UA_NumericRange *range, const UA_DataValue *data);

void afterWriteTime(UA_Server *server,
              const UA_NodeId *sessionId, void *sessionContext,
              const UA_NodeId *nodeId, void *nodeContext,
              const UA_NumericRange *range, const UA_DataValue *data);

void addValueCallbackToCurrentTimeVariable(UA_Server *server);

#endif /* CURRENTTIME_H */

